import grovepi
from google.cloud import logging
from google.cloud import error_reporting

import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/pi/FarmPi/creds.json"

#import logging
import datetime
import time
import json

#grolight_time= 57600  #light for 16 hours 
#grolight_relay=4

#pull light settings from json
with open('/home/pi/grolight.json') as f:
    d = json.load(f)
    grolight_time = d["on time"]
    grolight_relay = d["light relay"]


#pull configs
with open('/home/pi/FarmPi/farmpi/farmpi/farmpi.json') as f:
    d = json.load(f)
    grolight_relay = d["light relay"]

#logger = logging.getLogger('grolight')
#logger.setLevel(logging.INFO)
#fh = logging.FileHandler('/home/pi/FarmPi/logs/grolight.log')
#fh.setLevel(logging.INFO)
#logger.addHandler(fh)

logging_client = logging.Client()
log_name = "grolight"
logger = logging_client.logger(log_name)

#TO DO:  Add code to inquire moisture log and determine if soil is too wet
#logger.info('{"grolight":"on", "time":"%s", "sleep time":"%s"}', datetime.datetime.now(), grolight_time)
start_time_txt = datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")
logger_text = f'"grolight":"on", "time":"{start_time_txt}", "sleep time":"{grolight_time}"'
logger.log_text(logger_text)

grovepi.pinMode(grolight_relay,"OUTPUT")

#light on
grovepi.digitalWrite(grolight_relay,1)

#sleep and keep on
time.sleep(grolight_time)

#light off
#logger.info('{"grolight":"off", "time":"%s"}', datetime.datetime.now())
off_time_txt = datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")
logger_text = f'"grolight":"off", "time":"{off_time_txt}"'
logger.log_text(logger_text)

grovepi.digitalWrite(grolight_relay,0)
