#Author: Steve McAtee
#Created: 3/3/2020
#2/6/2021 Added Google Logging

import grovepi
#import logging
import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/pi/FarmPi/creds.json"

from google.cloud import logging
import datetime
import time
import json

sun_time=4   #4*60 

#sun_relay=4

#logger = logging.getLogger('sun')
#logger.setLevel(logging.INFO)
#fh = logging.FileHandler('/home/pi/FarmPi/logs/grolight.log')
#fh.setLevel(logging.INFO)
#logger.addHandler(fh)
logging_client = logging.Client()
log_name = "grolight"
logger = logging_client.logger(log_name)

#TO DO:  Add code to inquire moisture log and determine if soil is too wet
#logger.info('{"sun":"on", "time":"%s"}', datetime.datetime.now())

start_time_txt = datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")
logger_text = f'"grolight":"on", "time":"{start_time_txt}"'
logger.log_text(logger_text)

#pull configs
with open('/home/pi/FarmPi/farmpi/farmpi/farmpi.json') as f:
    d = json.load(f)
    light_relay = d["light relay"]

grovepi.pinMode(light_relay,"OUTPUT")

#water on
grovepi.digitalWrite(light_relay,1)

#sleep and keep on
time.sleep(sun_time)

#water off
start_time_txt = datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")
logger_text = f'"grolight":"off", "time":"{start_time_txt}"'
logger.log_text(logger_text)

#logger.info('{"sun":"off", "time":"%s"}', datetime.datetime.now())

grovepi.digitalWrite(light_relay,0)





