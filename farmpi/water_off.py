#Author: Steve McAtee
#Date: 3/1/2020
#Purpose:  Shutoff water
import grovepi
#import logging
import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/pi/FarmPi/creds.json"

from google.cloud import logging
import datetime
import json

#water_relay=2

#logger = logging.getLogger('water')
#logger.setLevel(logging.INFO)
#fh = logging.FileHandler('/home/pi/FarmPi/logs/water.log')
#fh.setLevel(logging.INFO)
#logger.addHandler(fh)
logging_client = logging.Client()
log_name = "water"
logger = logging_client.logger(log_name)

#pull configs
with open('/home/pi/FarmPi/farmpi/farmpi/farmpi.json') as f:
    d = json.load(f)
    water_relay = d["water relay"]

#TO DO:  Add code to inquire moisture log and determine if soil is too wet
#logger.info('{"water":"off", "time":"%s"}', datetime.datetime.now())
start_time_txt = datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")
logger_text = f'"water":"off", "time":"{start_time_txt}"'
logger.log_text(logger_text)

grovepi.pinMode(water_relay,"OUTPUT")

#water  off
grovepi.digitalWrite(water_relay,0)




