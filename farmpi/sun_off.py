#Author: Steve McAtee
#Date: 3/1/2020
#Purpose:  Shutoff water
#2/6/2021 Added Google Logging
import grovepi
from google.cloud import logging

import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/pi/FarmPi/creds.json"
#import logging
import datetime
import json


light_relay=4

#logger = logging.getLogger('sun')
#logger.setLevel(logging.INFO)
#fh = logging.FileHandler('/home/pi/FarmPi/logs/grolight.log')
#fh.setLevel(logging.INFO)

#logger.addHandler(fh)

logging_client = logging.Client()
log_name = "grolight"
logger = logging_client.logger(log_name)

#pull configs
with open('/home/pi/FarmPi/farmpi/farmpi/farmpi.json') as f:
    d = json.load(f)
    light_relay = d["light relay"]

#TO DO:  Add code to inquire moisture log and determine if soil is too wet
start_time_txt = datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")
logger_text = f'"grolight":"off", "time":"{start_time_txt}"'
logger.log_text(logger_text)

#logger.info('{"sun":"off", "time":"%s"}', datetime.datetime.now())

grovepi.pinMode(light_relay,"OUTPUT")

#water  off
grovepi.digitalWrite(light_relay,0)




