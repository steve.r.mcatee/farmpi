#Author: Steve McAtee
#Date: 3/1/2020
#Purpose:  Turn water on
#2/6/2021: Added Google Logging

import grovepi
from google.cloud import logging
import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/pi/FarmPi/creds.json"

#import logging
import datetime
import json

air_time=20*60

air_relay=3

logging_client = logging.Client()
log_name = "air"
logger = logging_client.logger(log_name)
#logger = logging.getLogger('air')
#logger.setLevel(logging.INFO)
#fh = logging.FileHandler('/home/pi/FarmPi/logs/air.log')
#fh.setLevel(logging.INFO)

#pull configs
with open('/home/pi/FarmPi/farmpi/farmpi/farmpi.json') as f:
    d = json.load(f)
    air_relay = d["air relay"]

    
#logger.addHandler(fh)

#TO DO:  Add code to inquire moisture log and determine if soil is too wet
#print(datetime.datetime.now())
logger_text = '{"air":"on", "time":"' + datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S") + '"}'
#print(logger_text)
logger.log_text(logger_text)
#logger.info('{"air":"on", "time":"%s"}', datetime.datetime.now())

grovepi.pinMode(air_relay,"OUTPUT")

#water on
grovepi.digitalWrite(air_relay,1)

