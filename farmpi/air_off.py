#Author: Steve McAtee
#Date: 3/1/2020
#Purpose:  Shutoff water
#Change: 11/13/2022 - Added config pull from json
import grovepi
from google.cloud import logging

import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/pi/FarmPi/creds.json"

#import logging
import datetime
import json

#air_relay=3

logging_client = logging.Client()
log_name = "air"
logger = logging_client.logger(log_name)
#logger = logging.getLogger('air')
#logger.setLevel(logging.INFO)
#fh = logging.FileHandler('/home/pi/FarmPi/logs/air.log')
#fh.setLevel(logging.INFO)

#logger.addHandler(fh)

#pull configs
with open('/home/pi/FarmPi/farmpi/farmpi/farmpi.json') as f:
    d = json.load(f)
    air_relay = d["air relay"]

#TO DO:  Add code to inquire moisture log and determine if soil is too wet
#logger.info('{"air":"off", "time":"%s"}', datetime.datetime.now())
logger_text = '{"air":"off", "time":"' + datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S") + '"}'
#print(logger_text)
logger.log_text(logger_text)

grovepi.pinMode(air_relay,"OUTPUT")

#water  off
grovepi.digitalWrite(air_relay,0)




