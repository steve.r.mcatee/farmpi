#Author: Steve McAtee
#Date: 3/1/2020
#Purpose:  Grab sensor data and log to file
import grovepi

from google.cloud import logging
from google.cloud import error_reporting

import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/pi/FarmPi/creds.json"

#import logging
#import logging.handlers
import datetime
import subprocess
import math

#analog sensor port number
mositure_sensor         = 1
light_sensor            = 2
dht_sensor_port = 8     # Connect the DHt sensor to port 7

logging_client = logging.Client()
log_name = "capture"
logger = logging_client.logger(log_name)

#LOG_FILENAME = "/home/pi/capture.log"
#logger = logging.getLogger('capture')
#logger.setLevel(logging.INFO)

#fh = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=6291456, backupCount=100)   #6MB

#fh = logging.FileHandler('/home/pi/setalarm.log')
#fh.setLevel(logging.INFO)

#logger.addHandler(fh)


try:
    [ temp,hum ] = grovepi.dht(dht_sensor_port,0)       #Get the temperature and Humidity from the DHT sensor
#    print("temp =", temp * 1.8 + 32, "F\thumidity =", hum,"%")  
    t = str(temp * 1.8 + 32)
    h = str(hum)
    
    moisture=grovepi.analogRead(mositure_sensor)
    
    light=grovepi.analogRead(light_sensor)
    logger_text = f'"time":"{datetime.datetime.now()}", "temp":"{t}", "humidity":"{h}", "light":"{light}", "moisture":"{moisture}"'
    logger.log_text(logger_text)
    
    #logger.info('{"time":"%s", "temp":"%s", "humidity:"%s", "light":"%s", "moisture":"%s"}', datetime.datetime.now(), t, h, light, moisture)
        
        
except Exception as e:
    print("error")
    error_client = error_reporting.Client()
    error_client.report("Error:" + str(e))
