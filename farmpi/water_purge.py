#Author: Steve McAtee
#Date: 3/1/2020
#Purpose:  Start watering

import grovepi
#import logging
import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/pi/FarmPi/creds.json"

from google.cloud import logging
import datetime
import time
import json
import sys

water_time=1
read_moisture_interval = 10
mositure_sensor=1
water_off_moisture_level = 200
#water_relay=2
OFF = 0
ON = 1

#pull moisture off level with config json
with open('/home/pi/wateroffmoisture.json') as f:
    d = json.load(f)
    water_off_moisture_level = d["water off moisture level"]
    read_moisture_interval = d["read moisture interval"]

    water_time= d["watering time"]
    water_time  = 30 * 60  #30 minute purge
 #   water_relay = d["water relay"]


#pull configs
with open('/home/pi/FarmPi/farmpi/farmpi/farmpi.json') as f:
    d = json.load(f)
    water_relay = d["water relay"]

#logger = logging.getLogger('water')
#logger.setLevel(logging.INFO)
#fh = logging.FileHandler('/home/pi/FarmPi/logs/water.log')
#fh.setLevel(logging.INFO)
#logger.addHandler(fh)
#logger.addHandler(logging.StreamHandler(sys.stdout))  #add console to handler
logging_client = logging.Client()
log_name = "water"
logger = logging_client.logger(log_name)

moisture=grovepi.analogRead(mositure_sensor)

start_time = datetime.datetime.now()
stop_time = start_time + datetime.timedelta(seconds=water_time)
#logger.info('{"water":"on", "moisture":"%s", "time":"%s", "stop time":"%s", "water relay":"%s"}', moisture, start_time, stop_time, water_relay)
start_time_txt = start_time.strftime("%m/%d/%Y %H:%M:%S")
stop_time_txt = stop_time.strftime("%m/%d/%Y %H:%M:%S")
logger_text = f'"water":"on", "moisture":"{moisture}", "time":"{start_time_txt}", "stop time":"{stop_time_txt}", "water relay":"{water_relay}"'
logger.log_text(logger_text)

#compute stop time for water

grovepi.pinMode(water_relay,"OUT")

#water on
grovepi.digitalWrite(water_relay,ON)

#loop until moisture exceeded or time exceeded
while moisture<water_off_moisture_level:
    moisture=grovepi.analogRead(mositure_sensor)
    time.sleep(read_moisture_interval)
    if datetime.datetime.now()>=stop_time:
        #waterring time reached turn off water
        break
    
#water off
grovepi.digitalWrite(water_relay,OFF)
#logger.info('{"water":"off", "moisture":%s, "time":"%s"}', moisture, datetime.datetime.now())

start_time_txt = datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")
logger_text = f'"water":"off", "moisture":"{moisture}", "time":"{start_time_txt}"'
logger.log_text(logger_text)
