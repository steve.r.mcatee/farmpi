#Author: Steve McAtee
#Date: 3/1/2020
#Purpose:  Start watering

import grovepi
from google.cloud import logging

import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/pi/FarmPi/creds.json"

#import logging

import datetime
import time
import json
import sys

air_time=1
air_relay=2
OFF = 0
ON = 1

#pull configs
with open('/home/pi/FarmPi/farmpi/farmpi/farmpi.json') as f:
    d = json.load(f)
    air_relay = d["air relay"]

logging_client = logging.Client()
log_name = "air"
logger = logging_client.logger(log_name)

#logger = logging.getLogger('air')
#logger.setLevel(logging.INFO)
#fh = logging.FileHandler('/home/pi/FarmPi/logs/air.log')
#fh.setLevel(logging.INFO)

#logger.addHandler(fh)
#logger.addHandler(logging.StreamHandler(sys.stdout))  #add console to handler

start_time = datetime.datetime.now()
stop_time = start_time + datetime.timedelta(seconds=air_time)
#logger.info('{"air":"on", "time":"%s", "stop time":"%s"}', start_time, stop_time)

start_time_txt = start_time.strftime("%m/%d/%Y %H:%M:%S")
stop_time_txt = stop_time.strftime("%m/%d/%Y %H:%M:%S")

logger_text = f'"air":"on", "time":"{start_time}", "stop time":"{stop_time_txt}"'


#logger_text = '{"air":"on", "time":"' + start_time.strftime("%m/%d/%Y %H:%M:%S") + '", "stop time":"' + stop_time.strftime("%m/%d/%Y %H:%M:%S") + '"}'
#print(logger_text)
logger.log_text(logger_text)

#compute stop time for water


grovepi.pinMode(air_relay,"OUTPUT")

#water on
grovepi.digitalWrite(air_relay,ON)

#loop until moisture exceeded or time exceeded
while datetime.datetime.now()<stop_time:
        #waterring time reached turn off water
	time.sleep(59)
#	logger.info(datetime.datetime.now())	#do nothing
#water off
grovepi.digitalWrite(air_relay,OFF)

start_time_txt = start_time.strftime("%m/%d/%Y %H:%M:%S")
stop_time_txt = stop_time.strftime("%m/%d/%Y %H:%M:%S")

logger_text = f'"air":"off", "time":"{start_time}", "stop time":"{stop_time_txt}"'
logger.log_text(logger_text)

#logger.info('{"air":"off", "time":"%s", "stop time":"%s"}', datetime.datetime.now(), stop_time)



